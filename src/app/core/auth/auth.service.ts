import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Auth } from './auth';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthService {
  data: Auth;
  error: HttpErrorResponse;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.data = JSON.parse(localStorage.getItem('data'));
  }

  login(username: string, password: string): void {
    this.error = null;
    const params = new HttpParams()
      .set('username', username)
      .set('password', password);

    this.http.get<Auth>('http://localhost:3000/login', { params })
      .subscribe(
        result => {
          this.data = result;
          this.router.navigateByUrl('admin');
          localStorage.setItem('data', JSON.stringify(this.data));
        },
        error => this.error = error
      );
  }

  logout(): void {
    localStorage.removeItem('data');
    this.data = null;
    this.router.navigateByUrl('login');
  }

  isLogged(): boolean {
    return  !!this.data;
  }

  /*getRole(): string {
    return this.data.role
  }*/

}
