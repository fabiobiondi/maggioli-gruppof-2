import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloneReq = req;
    if (this.authService.data) {
      cloneReq = req.clone({
        headers: req.headers.set('Auth', this.authService.data.token)
      });
    }
    return next.handle(cloneReq)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 404:
                alert('404');
                break;
              default:
              case 401:
              case 0:
                this.authService.logout();
                // this.router.navigateByUrl('login')
                break;
            }
          }
          // analizzare l'errore e fare qualcosa?? redirect, alert, ....
          // return observable?????
          // return of(null);
          return throwError(err);
        })
      );
  }
}


// -----o-------o-----o--------xo---o---oo------o-o-o--o--->
