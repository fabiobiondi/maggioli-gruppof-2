import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }
  canActivate(): boolean {
    if (!this.authService.isLogged()) {
      this.router.navigateByUrl('login')
    }
    return this.authService.isLogged();
  }

}
