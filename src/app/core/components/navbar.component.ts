import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'mg-navbar',
  template: `
    
    
    <nav class="navbar navbar-expand navbar-dark bg-dark text-white">

      <a class="navbar-brand"
         routerLink="/" routerLinkActive="active">LOGO</a>

      <div class="navbar-collapse collapse">
        
        <ul class="navbar-nav">
          <li class="nav-item"
              routerLink="/login" routerLinkActive="active"
              *ngIf="!authService.isLogged()"
          >
            <a class="nav-link">login</a>
          </li>

          <li class="nav-item"
              routerLink="/demo" routerLinkActive="active">
            <a class="nav-link">Demo</a>
          </li>

          <li class="nav-item"
              routerLink="/admin" 
              routerLinkActive="active"
              *ngIf="authService.isLogged()"
          >
            <a class="nav-link">admin</a>
          </li>
          
          <li class="nav-item" class="btn btn-primary" *ngIf="authService.data">
            {{authService.data?.displayName}}
          </li>
          
          <i 
            class="fa fa-times"
            *ngIf="authService.isLogged()"
            (click)="logoutHandler()"
          ></i>
          
        </ul>
      </div>
    </nav>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

  logoutHandler(): void {
    this.authService.logout();
  }
}
