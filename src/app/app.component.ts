import { Component } from '@angular/core';

@Component({
  selector: 'mg-root',
  template: `
    
    <mg-navbar></mg-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'maggioli-gf3';
}
