import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { BgDirective } from './directives/bg.directive';
import { PadDirective } from './directives/pad.directive';

@NgModule({
  declarations: [
    CardComponent, BgDirective, PadDirective
  ],
  exports: [
    CardComponent, BgDirective, PadDirective
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
