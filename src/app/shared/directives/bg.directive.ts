import { Directive, HostBinding, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[mgBg]'
})
export class BgDirective {
  @Input() mgBg = 'yellow';
  @HostBinding('style.backgroundColor') get bg(): string {
    return this.mgBg;
  }

}

/*

getElementById('pippo').innerHTML = 'dicaieifewfew';
getElementById('pippo').style.fontSize = '13px';
getElementById('pippo').className = 'active';
*/
