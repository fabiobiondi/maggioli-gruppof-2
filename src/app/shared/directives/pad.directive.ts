import { Directive, ElementRef, HostBinding, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[mgPad]',
})
export class PadDirective {
  @Input() set mgPad(val: 'sm' | 'md') {
    this.el.nativeElement.style.padding = val === 'sm' ? '10px' : '40px'
  }
  constructor(private el: ElementRef) {}
}

/*

getElementById('pippo').innerHTML = 'dicaieifewfew';
getElementById('pippo').style.fontSize = '13px';
getElementById('pippo').className = 'active';
*/
