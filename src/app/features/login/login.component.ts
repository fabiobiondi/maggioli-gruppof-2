import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'mg-login',
  template: `
    <div 
      class="alert alert-danger"
      *ngIf="authService.error"
      [ngSwitch]="authService.error?.status"
    >
      <div *ngSwitchCase="0">Not found</div>
      <div *ngSwitchCase="403">Username invalid</div>
    </div>
    
    <form [formGroup]="form" (submit)="login()">
      <input type="text" formControlName="username">
      <input type="text" formControlName="password">
      <button type="submit" [disabled]="form.invalid">Login</button>
    </form>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(fb: FormBuilder, public authService: AuthService) {
    this.form = fb.group({
      username: ['abc', Validators.compose([Validators.required, Validators.minLength(3)])],
      password: ['123456', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  login(): void {
    const { username, password } = this.form.value;
    this.authService.login(username, password);
  }
}
