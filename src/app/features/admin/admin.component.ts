import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'mg-admin',
  template: `
    <p>
      admin works!
      
      wefipjwepfjew
      
      ewf
      wefew
      
      
    </p>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(private http: HttpClient) {
    // Esempio chiamate multiple con gestione errore singola chiamata (posts)
    const users$ = this.http.get('http://localhost:3000/users');
    const posts$ = this.http.get('http://localhost:3000/postss').pipe(
      catchError(err => of([]))
    );

    forkJoin({ users: users$, posts: posts$ })
      .subscribe(
        val => console.log(val),
        err => console.warn(err)
      );
  }

  ngOnInit(): void {
    // esempio errore singola chiamata (vedi interceptor throwError)
    this.http.get('http://localhost:3000/users')
      .subscribe(
        result => {
          console.log('result', result)
        },
        error => console.log('Admin component users API error')
      );
  }

}
