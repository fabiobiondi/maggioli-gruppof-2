import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-demo',
  template: `
    <input type="text" [ngModel]>
    <span [mgBg] mgPad="sm">a</span>
    <span mgPad="md">b</span>
    <span [mgBg]="color" mgPad>c</span>
    <span mgPad>d</span>
    <hr>
    <button (click)="color = 'blue'">blue</button>

    <mg-card></mg-card>
    <mg-card></mg-card>
    <mg-card></mg-card>
    
  `,
  styles: [
  ]
})
export class DemoComponent implements OnInit {
  color = 'red';
  constructor() { }

  ngOnInit(): void {
  }

}
